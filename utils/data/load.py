"""
Arquivo que comunica diretório com sinais em txt com vetores numpy separados
por classe.
"""
from re import split
import numpy as np
from os import listdir as l
from scipy.spatial import distance_matrix

class DataLoader():
    def __init__(self, master_dir=None, max_signal_length=None, positive_class=None):
        self.master_dir = master_dir
        self.positive_class = positive_class
        self.max_signal_length = max_signal_length
        self.signals = None
        self.labels = None
        self.data = None

    def set(self, master_dir, max_signal_length, positive_class=None):
        self.master_dir = master_dir
        self.positive_class = positive_class
        self.max_signal_length = max_signal_length

    def batch_dataload(self):

        if self.positive_class is None:
            print("Classe positiva não informada")

        positive_class = self.positive_class

        classes = l(self.master_dir)

        if positive_class not in classes:
            print("Classe ausente")

        swp_index = classes.index(self.positive_class)

        classes[swp_index] = classes[1]
        classes[1] = positive_class

        class_dict = {}
        for c, clss in enumerate(classes):
            class_dict[clss] = c


        data = self.class_dataload(self.master_dir+"/"+classes[0], 0)

        for c, cls in enumerate(classes[1:]):
            cpath = self.master_dir+"/"+cls
            class_data = self.class_dataload(cpath, c+1)
            data = np.vstack((data, class_data))

        self.data = np.nan_to_num(data)
        self.class_dict = class_dict
        self.inv_class_dict = {k: v for v, k in class_dict.items()}

        # Class dictionaries are confusing, so some comments are in order:
        #   class_dict assigns an integer value to each class name, so it
        #   is accessed as such:
        #       class_dict["dengue"] == 0
        #
        #   inv_class_dict therefore inverts this relationship as such:
        #       inv_class_dict[0] == "dengue"
        #

    def class_dataload(self, cls_pth, label):

        files = l(cls_pth)
        data = self.dataload(cls_pth+"/"+files[0])

        for f, fl in enumerate(files): # beware of enumerate!
            if f != 0:
                row = self.dataload(cls_pth+"/"+fl)
                data = np.vstack((data, row))

        rs = (data.shape[0], 1)
        labels = np.full(shape=rs, fill_value=label, dtype=np.int)
        data = np.concatenate((data, labels), axis=1)

        return data

    def dataload(self, pth):

        i = []

        with open(pth, encoding='utf-8') as f:
            for line in f:
                numeric_data = split("[ \t]", line)
                if numeric_data[-1] != "" and numeric_data[-1] != "\n":
                    i.append(float(numeric_data[-1].replace("\n", "")))

        i = np.array(i)

        expected_size = self.max_signal_length

        if expected_size is not None:  # Padding
            if len(i) != expected_size:
                if len(i) < expected_size:
                    i = np.pad(i, ((expected_size - len(i)), 0))
                else:
                    i = i[:(expected_size - len(i))]

        return i

    def format(self):
        self.signals = self.data[:, :-1]
        self.labels = self.data[:, -1]

    def clip_signals(self, samples, first=True):

        if self.data is None:
            assert "Data not loaded. Run DataLoader.batch_dataload() first"
        if self.signals is None:
            assert "Data not formatted. Run DataLoader.format() first"

        if first:
            self.signals = self.signals[:, samples:]
        else:
            self.signals = self.signals[:, :-samples]

    # def remove_outliers(self): # TODO: finish this shit self.dist = (self.signals)
    #     self.mean_dist = np.mean(dist)
