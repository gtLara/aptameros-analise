"""
Script de análise visual em alto nível
"""
from utils.visual.tools import plot_signals, scatter_signals, similarity, similarity_plot
from matplotlib import pyplot as plt
import numpy as np

def signal_similarity_plot(signals, labels, forced='completo', metric=None,
                           title=None, save=False, show=False,
                           fp="assets/visual/total/",
                           fn="heat.png"):

    plt.close('all')

    ord_signals = signals[labels.argsort()]

    if forced == 'completo':
        ord_signals = np.concatenate((ord_signals, labels.reshape((len(labels), 1))), axis=1)
    elif forced == 'dicotomia':
        labels = np.array([l if l == 1 else 0 for l in labels])
        ord_signals = np.concatenate((ord_signals, labels.reshape((len(labels), 1))), axis=1)

    # print(ord_signals.shape)
    similarity_plot(ord_signals, title=title, save=save, fn=fp+fn, metric=metric)

    if show:
        plt.show()

def multiscatter(signals, labels, class_dict, complete=True, show=False, save=False, fp=None, grid=False, seed=None, cuttoff=None):

    plt.close('all')

    methods = ['tsne', 'ica', 'pca', 'mds']
    reduced_coordinates = {}

    plt.subplot(221)
    reduced_coordinates[methods[0]] = scatter_signals(signals, labels, class_dict, method=methods[0], title=methods[0].upper(), show=False, legend=False, complete=complete, grid=grid, seed=seed, cuttoff=cuttoff)

    plt.subplot(222)
    reduced_coordinates[methods[1]] = scatter_signals(signals, labels, class_dict, method=methods[1], title=methods[1].upper(), show=False, legend=False, complete=complete, grid=grid, seed=seed, cuttoff=cuttoff)

    plt.subplot(223)
    reduced_coordinates[methods[2]] = scatter_signals(signals, labels, class_dict, method=methods[2], title=methods[2].upper(), show=False, legend=False, complete=complete, grid=grid, seed=seed, cuttoff=cuttoff)

    plt.subplot(224)
    reduced_coordinates[methods[3]] = scatter_signals(signals, labels, class_dict, method=methods[3], title=methods[3].upper(),  show=show, legend=True, complete=complete, grid=grid, seed=seed, cuttoff=cuttoff)

    if save:
        plt.savefig(fp)
        plt.close()

    return reduced_coordinates
