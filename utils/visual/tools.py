"""
Conjunto de ferramentas de visualização
"""
import matplotlib.pyplot as plt
import numpy as np
from sklearn.manifold import TSNE
from sklearn.manifold import MDS
from sklearn.decomposition import PCA
from sklearn.decomposition import FastICA
from mpl_toolkits.mplot3d import Axes3D
from scipy.spatial import distance

cd={
    0:'Aptâmero',
    1:'Zika',
    2:'Dengue',
    3:'CEA'
    }

color_map={ 0:'black',
            1:'red',
            2:'green',
            3:'blue',
            4:'orange',
            5:'purple',
            6:'yellow',
            7:'brown',
            8:'navy',
            9:'pink',
            10:'grey'
            }

def plot_signals(signals, labels, class_dict=cd, xl="t(-)", yl="I(A)", title=None, sep=False, save=False, fp="assets/visual/simple/", show=False):

    labels = [int(l) for l in labels]
    inv_cd = {k: v for v, k in class_dict.items()}

    if sep:
        for n, cls in enumerate(class_dict):
            for signal in signals[np.where(labels==n)]:
                plt.plot(signal, color=color_map[n])
                plt.title(inv_cd[cls])

            if save: plt.savefig(fp+inv_cd[cls]+"-signals.png")

            if show: plt.show()

    else:

        oldc = -1
        for s, signal in enumerate(signals):
            if labels is not None:
                cid = labels[s]
                if oldc != cid:
                    plt.plot(signal, color=color_map[cid], label=inv_cd[cid])
                else:
                    plt.plot(signal, color=color_map[cid])
                oldc=cid
            else:
                plt.plot(signal, color='black')

        plt.title(title)
        plt.legend()
        plt.xlabel(xl)
        plt.ylabel(yl)

        if save:
            plt.savefig(fp+"signals.png")

        if show: plt.show()

def remove_outliers(signals, cuttoff):

    init_len = signals.shape[0]

    if not cuttoff:
        return signals

    centroid = np.mean(signals, axis=0)
    dists = [distance.euclidean(centroid, signal) for signal in signals]

    mean_dist = np.mean(dists)
    std = np.std(dists)

    keep = np.where(dists < (cuttoff*std) + mean_dist)

    return keep

def scatter_signals(signals, labels, cls_dict, seed, method=None, dims=3, title=None, legend=True, show=True, complete=True, save=False, fp="assets/visual/partial/", grid=False, cuttoff=None):

    # print(f"seed:{seed}")

    reduce = {
                'tsne': TSNE,
                'mds': MDS,
                'pca': PCA,
                'ica': FastICA
             }

    if method:
        reduced_signals = reduce[method](n_components=dims, random_state=seed).fit_transform(signals)
    else:
        reduced_signals = signals

    if cuttoff:

        # print(f"cut:{cuttoff}")

        initial_size = len(reduced_signals)

        keep = remove_outliers(reduced_signals, cuttoff)

        reduced_signals = reduced_signals[keep]
        labels = labels[keep]

        drops = initial_size - len(keep[0]) # np.where retorna uma tupla!
        # print(f"file:{method}:{initial_size}:{len(keep[0])}:{reduced_signals.shape[0]}")

    if not complete:

        pos = np.where(labels==1)
        neg = np.where(labels==0)

        plot_label = 'Negativo' if legend else None

        plt.scatter(reduced_signals[neg, 0],
                    reduced_signals[neg, 1],
                    c = 'black',
                    label = plot_label)

        plot_label = 'Positivo' if legend else None

        plt.scatter(reduced_signals[pos, 0],
                    reduced_signals[pos, 1],
                    c = 'red',
                    label = plot_label)

        for p in pos[0]:
            plt.text(reduced_signals[p, 0],
                     reduced_signals[p, 1],
                     p,
                     fontsize = 9)

        for n in neg[0]:
            plt.text(reduced_signals[n, 0],
                     reduced_signals[n, 1],
                     n,
                     fontsize = 9)

    else:


        for c, cls in enumerate(cls_dict):

            idx = np.where(labels == cls_dict[cls])
            x = reduced_signals[idx, 0]
            y = reduced_signals[idx, 1]

            plt.scatter(x,
                        y,
                        c = color_map[c],
                        label = cls)

    # mega gambiarra: plotta um ponto invisível e assinala como label
    # o número de outliers para que isso seja mostrado como informação na
    # legenda

    if cuttoff:
        plt.scatter(reduced_signals[0, 0], reduced_signals[0, 0], alpha=0, label=f"Outliers:{drops}")
        plt.legend()

    if legend:plt.legend()

    plt.title(title)

    if save: plt.savefig(fp+"scatter-"+str(complete).lower())
    if grid: plt.grid()
    else:
        plt.yticks([])
        plt.xticks([])

    if show:
        plt.show(block=True)

    return reduced_signals

def similarity(A_vecs, B_vecs, metric=None):

    s = A_vecs.shape[0]
    c = B_vecs.shape[0]

    similarity = np.zeros((s, c))

    for si, svec in enumerate(A_vecs):
        for ci, cvec in enumerate(B_vecs):
            if metric:
                if metric == np.cross: # generalized cross product
                    similarity[si, ci] = np.linalg.norm(metric(svec, cvec))
                elif metric == np.correlate: # cross correlation
                    svec = (svec - np.mean(svec)) / (np.std(svec) * len(svec))
                    cvec = (cvec - np.mean(cvec)) / (np.std(cvec))
                    similarity[si, ci] = np.mean(metric(svec, cvec, "full"))
            else:
                similarity[si, ci] = distance.euclidean(svec, cvec)

    return similarity

def similarity_plot(a, b=None, show=False, xl=None, yl=None, title=None, sim=None, cmap='bone', save=False, fn='simplot.png', orientation='vertical', metric=None):

    plt.close('all')

    if b is None:
        b = a

    if sim is None:
        sim = similarity(a, b, metric=metric)

    plt.imshow(sim, cmap=cmap)

    plt.title(title)
    plt.xlabel(xl)
    plt.ylabel(yl)
    if orientation is not None: plt.colorbar(orientation=orientation)

    plt.yticks([])
    plt.xticks([])

    if save: plt.savefig(fn)
    if show: plt.show(block=True)
