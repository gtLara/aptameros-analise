import sys
sys.path.append("../..")

import pandas as pd
import numpy as np
from utils.data.load import DataLoader

class DataSummary(DataLoader): # TODO: consertar isso
    def split_by_class(self):
        self.class_split = []
        for c in range(len(self.class_dict)):
            self.class_split.append(self.signals[np.where(self.labels == c)])

    def set_mean(self):
        self.mean = [round(np.mean(signals), 2) for signals in self.class_split]

    def set_std(self):
        self.std = [round(np.mean(np.std(signals, axis = 1)), 2) for signals in self.class_split]

    def set_sum(self):
        self.sum = [round(np.mean(np.sum(signals, axis = 1)), 2) for signals in self.class_split]

    def set_counter(self):
        self.count = [len(signals) for signals in self.class_split]

    def set_diff(self):
        self.diff = [np.mean(np.diff(signals, axis=1)) for signals in self.class_split]

    def set_summary(self):
        self.set_diff()
        self.set_counter()
        self.set_sum()
        self.set_mean()
        self.set_std()

    def dump_to_csv(self, fn="data_summary.csv"):

        data = {"Média":self.mean,
                "Desvio Padrão":self.std,
                "Média de Integral":self.sum,
                "Número de Sinais":self.count,
                "Média de Derivada":self.diff}

        df = pd.DataFrame(data, index = self.class_dict.keys())

        df.to_csv(fn)
