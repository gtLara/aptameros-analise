# Guia de Uso: Analisador de Dados

O seguinte documento instrui brevemente o uso do programa de análise de dados desenvolvido para o projeto dos aptâmeros. Para evitar erros do processo de porte da interface gráfica para a plataforma Windows optou-se por incluir nesse manual etapas para a execução do código diretamente de sua fonte. Tal proximidade é desejável também  para permitir interação do usuário com o código (que é livre no contexto do projeto).

## Instalação e Preparo de Ambiente *Python*

O código é desenvolvido e executado em *Python* e a interface gráfica pode ser executada por  qualquer versão acima de 3.6. Esta seção instrui a instalação do *miniconda*, uma plataforma que permite a criação de ambientes virtuais * onde a manipulação de *Python*, suas versões e pacotes é intuitivo.

### Instalação: *Miniconda*

 1. Baixe o instalador de *miniconda* (python 3.8) apropriado a seu sistema no link abaixo

https://docs.conda.io/en/latest/miniconda.html

2. Instale o *miniconda*
3. Pesquise na barra de pesquisa do Windows e abra o "Anaconda Prompt"
4. Crie um ambiente virtual* com o seguinte comando:


    `conda create -n "nome_de_seu_ambiente" python=3.8`

	Observe que o campo entre aspas é arbitrário

5. Ative o ambiente com o seguinte comando:

    `conda activate "nome_de_seu_ambiente"`

6. Instale os pacotes relevantes para a execução desse programa com o seguinte comando (é possível também instalar esses pacotes como informado na próxima seção):

    `pip3 install numpy=1.19.3`

    `pip3 install sklearn matplotlib pandas`

------------------------------------------------------

O ambiente está preparado. O programa deve agora ser executado no prompt do *anaconda* e com o ambiente ativo.

\* um ambiente virtual é um espaço virtual criado para que uma versão de *Python* e seus pacotes sejam controlados de forma isolada, sem interagir com a base de seu sistema

### Execução de Programa

1.	Baixe o programa a partir deste repositório

![git-download](assets/git_download.png?raw=true)

2. Descomprima o arquivo
3. Abra seu prompt *anaconda*
4. Ative seu ambiente conda
5. Navegue, no prompt, até a pasta extraída com o comando *[cd](https://medium.com/@adsonrocha/como-abrir-e-navegar-entre-pastas-com-o-prompt-de-comandos-do-windows-10-68750eae8f47)*
6. Se não tiver instalado os pacotes relevantes durante a criação do ambiente, instale-os com o seguinte comando:
`pip3 install -r requirements.txt`
7. Execute o arquivo *gui* com o seguinte comando:
 `python gui.py`

## Uso de Analisador

O uso do programa em si é dado em poucos passos. É importante se atentar ao formato dos dados que deve ser como a pasta `batch_1` e `batch_2`, contida no arquivo baixado no repositório, exemplifica: uma pasta mestre contendo os sinais em .txt dividido entre classes.

 1. Defina uma classe positiva: A classe positiva é a classe de maior interesse para a detecção. No caso de um dos dados de exemplo nesse repositório essa classe é a "zika".

 2.  Defina um tamanho esperado de sinal: Os sinais de entrada possuem um tamanho ou número de amostras do qual raramente se varia, mas é interessante que o programa seja robusto à eventuais variações. Para tal, informa-se o número esperado de amostras por sinal no campo correspondente (1001 é o tamanho esperado dos sinais dos dados de `batch_2`)
 3. Aponte para a pasta dos dados
![apontar](assets/apontar_dados.png?raw=true)
4. Carregar dados
![carregar](assets/carregar_dados.png?raw=true)
5. Verificar "OK" verde
![ok](assets/ok.png?raw=true)
6. Gerar imagens desejadas. É possível escolher os parâmetros relevantes para a imagem, como a representação dicotômica ou não dos dados baseado na classe positiva. Para explicação precisa do significado de cada opção favor referir às seções de visualização da documentação de análise exploratória de dados entregue ao projeto. As imagens podem ser salvas em seu visualizador.
![see](assets/visualizacao.png?raw=true)

### Não apareceu o OK verde! Como proceder?
Confira se a organização dos dados está coerente com a organização exemplificada nos dois arquivos `batch` presentes no repositório. Confira também se os arquivos .txt dos sinais estão no formato semelhante dos .txt desses arquivos de exemplo. Confira em seguida se o passo-a-passo foi seguido corretamente. Se o problema persistir, entre em contato com o desenvolvedor no e-mail presente no fim desse documento.

### Observações
O programa é para uso dos integrantes do projeto e seu desenvolvimento está aberto às sugestões de todos. Sugestões sobre
entrada de dados, representações numéricas e/ou adicionais e relatos de bugs podem ser enviados para o email gtlchaves@ufmg.br
