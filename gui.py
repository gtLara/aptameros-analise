from sklearn.manifold import TSNE
from scipy.spatial import distance
from sklearn.manifold import MDS
from sklearn.decomposition import PCA
from sklearn.decomposition import FastICA
from tkinter import *
from tkinter import filedialog
from utils.visual.wrap import *
from utils.numeric.tools import DataSummary
from utils.visual.tools import remove_outliers
import pandas as pd
import random

seed = random.randint(0, 1000000)
methods = ['tsne', 'ica', 'pca', 'mds']

class Data(DataSummary):

    def check_integrity(self):
        if len(self.labels) == self.signals.shape[1]: pass
        return "OK"

    def clear(self):
        self.data = None
        self.signals = None
        self.labels = None


def browse_files():
    filename = filedialog.askdirectory(initialdir = initial_dir,
                                          title = "Selecione um arquivo")

    file_explorer.configure(text=filename)

def load_data():

    data_holder.clear()

    response_label.configure(text="")

    positive_class = positive_class_entry.get()
    expected_signal_len = int(expected_signal_len_entry.get())
    dp = file_explorer.cget("text")

    data_holder.set(dp, expected_signal_len, positive_class)

    data_holder.batch_dataload()

    data_holder.format()

    response = data_holder.check_integrity()

    if response: color = 'green'
    else: color = 'red'

    if response: response_text = "OK"
    else: response_text = "Erro"

    response_label.configure(text=response_text, fg=color)

def similarity_matrix():

    cuttoff = slider.get()

    types = []
    titles = []

    if check_1.get():
        titles.append("Normal")
        types.append(None)
    if check_2.get():
        titles.append("Dicotômica")
        types.append("dicotomia")
    if check_3.get():
        titles.append("Completa")
        types.append("completo")

    keep = remove_outliers(data_holder.signals, cuttoff)
    signals = data_holder.signals[keep]
    labels = data_holder.labels[keep]

    for t, tt in zip(types,titles):
        signal_similarity_plot(signals, labels, forced=t, show=True, title="Análise de Semelhança "+tt)

def scatter():
    cuttoff = slider.get() if slider.get() != 0 else None
    multiscatter(data_holder.signals, data_holder.labels, data_holder.class_dict, complete=check_4.get(), show=True, grid=True, seed=seed, cuttoff=cuttoff)

def numeric_dump():
    data_holder.split_by_class()
    data_holder.set_summary()
    data_holder.dump_to_csv(fn="resumo.csv")

def create_label_file():

    cuttoff = slider.get() #if slider.get() != 0 else None

    #TODO: fazer com que essa funcao use o mesmo conjunto de sinais reduzidos
    # que a funcao de plotar os sinais reduzidos!
    # dessa forma esta mega ineficiente e vai dar merda pra datasets maiores

    coord_dict = {}

    coord_dict['classe'] = [data_holder.inv_class_dict[l] for l in data_holder.labels]

    reduce = {
                'tsne': TSNE,
                'mds': MDS,
                'pca': PCA,
                'ica': FastICA
             }

    for mth in methods:

        reduced_signals = reduce[mth](n_components=2, random_state=seed).fit_transform(data_holder.signals)

        # if cuttoff:
        #     print(f"cut:{cuttoff}")
        #     init_len = reduced_signals.shape[0]
        #     keep = remove_outliers(reduced_signals, cuttoff)
        #     reduced_signals = reduced_signals[keep]
        #     drops = init_len - len(keep[0])
        #     print(f"file:{mth}:{init_len}:{len(keep[0])}:{reduced_signals.shape[0]}")

        coord_dict[mth] = reduced_signals
        coord_dict[mth] = [str(round(c[0], 4)) + " " + str(round(c[1], 4)) for c in coord_dict[mth]]

    # padding: ok
    max_size = len(coord_dict["classe"])

    for method, coord in coord_dict.items():
        padded_coords = coord
        for p in range(max_size - len(coord)):
            padded_coords.append(None)

        coord_dict[method] = padded_coords

    df = pd.DataFrame(coord_dict)

    df.to_csv("labels.csv")

###############################################################################

window = Tk()

window.geometry('900x400')

# style = ThemedStyle(window) # aesthetic stuff
# style.set_theme("Keramic")

window.title("Analise Visual")

initial_dir = "/home/gala/git/pessoal/aptameros/data"


data_holder = Data()
coord = None

################################################################################Misc

slider = Scale(window, from_=0, to=50, orient=HORIZONTAL, length=175, font=("Helvetica", "0"), width=25, label="Sensibilidade")
slider.set(7)

file_explorer = Label(window,
                      text = "Caminho para Dados",
                      width = 50, height = 4, font=("Helvetica", 18))

similarity_types = {'Dicotômico', 'Completo', 'Normal'}

check_1, check_2, check_3 = IntVar(), IntVar(), IntVar() #similarity checkboxes

normal_check = Checkbutton(window, text = "Normal", font=("Helvetica", 18), variable = check_1)
dicotomy_check = Checkbutton(window, text = "Dicotômica", font=("Helvetica", 18), variable = check_2)
complete_check = Checkbutton(window, text = "Completa", font=("Helvetica", 18), variable = check_3)

check_4 = IntVar() #scatter checkbox

scatter_status_check = Checkbutton(window, text = "Completa", font=("Helvetica", 18), variable = check_4)

################################################################################Buttons

file_button = Button(window, text="Definir Caminho de Dados", command=browse_files, font=("Helvetica", 18))
load_button = Button(window, text="Carregar Dados", command=load_data, font=("Helvetica", 18))
# similarity_matrix_button = Button(window, text="Gerar", command=similarity_matrix, font=("Helvetica", 18))
similarity_matrix_button = Button(window, text="Matriz de Semelhança", command = similarity_matrix,font=("Helvetica", 20))
scatter_button = Button(window, text="Dispersão 2D", command = scatter, font=("Helvetica", 20))
label_file_button = Button(window, text="Gerar Arquivo de Rótulos", command = create_label_file, font=("Helvetica", 20))
# summary_button = Button(window, text="Gerar Análise Numérica", command = numeric_dump, font=("Helvetica", 20))

################################################################################Labels

emp = Label(window, text="", font=("Helvetica", 48))
emp1 = Label(window, text="", font=("Helvetica", 48))
emp2 = Label(window, text="", font=("Helvetica", 48))
emp3 = Label(window, text="", font=("Helvetica", 48))
emp4 = Label(window, text="", font=("Helvetica", 48))
file_path_label = Label(window, text="Insira Caminho de Dados", font=("Helvetica", 20))
positive_class_label = Label(window, text="Classe Positiva:", font=("Helvetica", 20))
expected_signal_len_label = Label(window, text="Tamanho Esperado de Sinal:", font=("Helvetica", 20))
response_label = Label(window, text="", font=("Helvetica", 20))
#similarity_matrix_label = Label(window, text="Matriz de Semelhança", font=("Helvetica", 20))

################################################################################Entries

positive_class_entry = Entry(window, width=13, font=("Helvetica", 20))
expected_signal_len_entry = Entry(window, width=13, font=("Helvetica", 20))

################################################################################Grid

file_button.grid(column=0, row=0)
file_explorer.grid(column=2, row=0)

load_button.grid(column=0, row=1)
positive_class_label.grid(column=1, row=1)
positive_class_entry.grid(column=2, row=1)
#response_label.grid(column=3, row=1)

emp.grid(column=0, row=2)
expected_signal_len_label.grid(column=1, row=2)
expected_signal_len_entry.grid(column=2, row=2)

similarity_matrix_button.grid(column=0, row=3)
response_label.grid(column=2, row=3)

emp1.grid(column=0, row=4)

normal_check.grid(column=0, row=5)
dicotomy_check.grid(column=1, row=5)
complete_check.grid(column=2, row=5)

emp2.grid(column=0, row=6)

scatter_button.grid(column=0, row=7)
scatter_status_check.grid(column=1, row=7)
slider.grid(column=2, row=7)

emp3.grid(column=0, row=8)
emp4.grid(column=1, row=8)
label_file_button.grid(column=0, row=9)
# summary_button.grid(column=1, row=9)

window.mainloop()
