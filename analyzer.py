"""
Feature analyzer
"""
import matplotlib.pyplot as plt
import numpy as np
from numpy.random import RandomState
# from sklearn.cluster import MiniBatchKMeans
from utils.data.load import DataLoader
from sklearn import decomposition
from sklearn.preprocessing import StandardScaler
from utils.visual.wrap import multiscatter, signal_similarity_plot
from utils.visual.tools import scatter_signals


class Analyzer(DataLoader):
    def __init__(self, n_components, master_dir=None, max_signal_length=None,
                 positive_class=None, standard=False):

        self.standard = standard
        self.rng = RandomState(0)
        self.estimates = {}
        self.estimators = [

                ("PCA", decomposition.PCA(n_components=n_components,
                 svd_solver='randomized')),

                ("NMF", decomposition.NMF(n_components=n_components,
                 init='nndsvda', tol=5e-3)),

                # ("FastICA", decomposition.
                #  FastICA(n_components=n_components)),

                ("MBSPCA", decomposition.MiniBatchSparsePCA(
                 n_components=n_components, alpha=0.8,
                 n_iter=100, batch_size=3)),

                ("MBDL", decomposition.MiniBatchDictionaryLearning(
                 n_components=15, alpha=0.1,
                 n_iter=50, batch_size=3,
                 random_state=self.rng)),

                # ("MBKMeans", MiniBatchKMeans(n_clusters=n_components, tol=1e-3,
                #  batch_size=20,
                #  max_iter=50, random_state=self.rng)),

                # ("FA", decomposition.FactorAnalysis(n_components=n_components,
                #  max_iter=20)),

                ]

        self.non_negative_signals = None

        DataLoader.__init__(self, master_dir, max_signal_length,
                            positive_class)

    # TODO: recuperar valores quantitivos sobre dados com funcao abaixo
    def quantify(self):
        pass

    # TODO: refletir sobre regularização
    # Não normalizar + NMF + matriz completa quase elimina sinais anomalos
    # podendo incidar que regularização PIORA desempenho.

    def preprocess(self):
        if self.signals is None:
            assert "Data not formatted"

        # self.signals = StandardScaler().fit_transform(self.signals)
        non_negative_signals = self.signals
        non_negative_signals[self.signals < 0] = 0
        self.non_negative_signals = non_negative_signals

    def get_features(self, method=None):

        for name, estimator in self.estimators:

            if method:
                if name == method:
                    data = self.signals
                    reduced_signals = estimator.fit_transform(data)
                    self.estimates[name] = reduced_signals
                else:
                    pass
            else:
                if name == "NMF":
                    if self.non_negative_signals is None:
                        assert "Data not preprocessed"
                    data = self.non_negative_signals
                else:
                    data = self.signals
                reduced_signals = estimator.fit_transform(data)
                self.estimates[name] = reduced_signals

    def scatter_features(self, estimator):
        self.get_features(estimator)
        signals = self.estimates[estimator]
        scatter_signals(signals, self.labels, self.class_dict)

    def distance_matrix(self, estimator=None, forced="completo"):

        if estimator:
            self.get_features(estimator)
            signals = self.estimates[estimator]
        else:
            signals = self.signals

        signal_similarity_plot(signals, self.labels, forced=forced,
                               show=True)

    def product_analysis(self, forced="normal", method="NMF"):
        plt.subplot(121)
        #TODO: normalizar dados antes de gerar matriz de semelhança
        signal_similarity_plot(self.signals, self.labels, forced=forced,
                               metric=np.dot, title=r"S . S")
        plt.subplot(122)

        self.get_features(method)
        signals = self.estimates[method]
        signal_similarity_plot(signals, self.labels, forced=forced,
                               metric=np.cross, title=r"S x S")

        plt.show()
